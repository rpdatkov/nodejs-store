var selector     = document.getElementById("currency");
var option       = selector.options[selector.selectedIndex];

var productPrice = document.getElementById("prod-price").value;
var getValue     = document.getElementById("valueResult").value;
var fullPath     = window.location.pathname.toString();

//USD currency value
var usd          = 4.1722;

//gets the Currency type
var currencyType = fullPath.slice(window.location.pathname.length-3, window.location.pathname.length);
var path         = fullPath.slice(0, window.location.pathname.length-3);

//perform check if the currency is USD
if(currencyType != 'USD') {
    document.getElementById("prod-price").value = productPrice * (usd/getValue) ;
}

console.log(getValue);
console.log(productPrice);
console.log(currencyType);
console.log(selector.options[selector.selectedIndex].value);

//set the SELECT element SELECTED option depending on the selected currency
if(selector.options[selector.selectedIndex].value != currencyType) {
    switch(currencyType) {
        case 'USD': 
            selector.selectedIndex = 0;
            break;
        case 'EUR':
            selector.selectedIndex = 1;
            break;
        case 'GBP':
            selector.selectedIndex = 2;
            break;
        case 'BGN':
            selector.selectedIndex = 3;
            break;
    }
}

selector.addEventListener('change', function() {
    option = selector.options[selector.selectedIndex];
    path   = fullPath.slice(0, window.location.pathname.length-3);
    window.location.pathname = path + option.value;
});




