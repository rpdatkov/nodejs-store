exports.index = function(req, res) {
	res.render("index", { 
		// Template data
		title: "Express" 
	});
};

//Include SOAP
var soap    = require("soap");
var url     = 'http://infovalutar.ro/curs.asmx?wsdl';

exports.hello = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var id = req.params.id;
    
	mdbClient.connect("mongodb://localhost:27017/Shop", function(err, db) {
		var products = db.collection('Categories');        
        
		products.find().toArray(function(err, id) {
			res.render("hello", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Node JS Store",                
				id : id
			});
        db.close();
	});
})};

exports.category = function(req, res) {
    var _         = require("underscore");
    var mdbClient = require("mongodb").MongoClient;
    var id = req.params.id;
    
    mdbClient.connect("mongodb://localhost:27017/Shop", function(err, db) {
		var products = db.collection('Categories');
        
		products.find({id:id}).toArray(function(err,id) {
			res.render("category", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Category",         
                id    : id
			});
        db.close();
	});
})};

exports.subcategory = function (req, res) {
    var _         = require("underscore");
    var mdbClient = require("mongodb").MongoClient;
    var id = req.params.id;
    var subid = req.params.subid;
    
    mdbClient.connect("mongodb://localhost:27017/Shop", function(err, db) {
		var products = db.collection('Categories');
        
		products.find({id:id},{"categories":req.params.subid}).toArray(function(err,id, subid) {
			res.render("subcategory", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Subcategory",         
                id    : id,
                previd : req.params.id,
                subid : req.params.subid
			});
        db.close();
	});
})};

exports.products = function(request, response) {
    var _        = require("underscore");
    var mbClient = require('mongodb').MongoClient;
    var id = request.params.id;
    var subid = request.params.subid;
    var prosid = request.params.prosid;
    
    mbClient.connect("mongodb://localhost:27017/Shop", function(err, db) {
        var products = db.collection("Products");
        
        products.find().toArray(function(err, items, id, subid, prosid) {
            response.render("products", {
                _    : _,
                title: "Products",
                items: items,
                id   : id,
                subid: request.params.subid,
                prosid:request.params.prosid,
                previd: request.params.id
            });
            db.close();
        });
})};

exports.product  = function(req, res) {
    var _        = require("underscore");
    var mbClient = require('mongodb').MongoClient;
    var id       = req.params.id;
    var subid    = req.params.subid;
    var prosid   = req.params.prosid;
    var prodid   = req.params.prodid;
    var curr     = req.params.curr;
    var result   = 0;
    //SOAP
        
    mbClient.connect("mongodb://localhost:27017/Shop", function(err, db) {
        var products = db.collection("Products");
        soap.createClient(url, function(err,client) {    
            client.getlatestvalue({Moneda:req.params.curr}, function(err, result) {
            products.find({id:prodid}).toArray(function(err, items, id, subid, prodid) {
                res.render("product", {
                    _     : _,
                    title : "Product",
                    items : items,
                    id    : id,
                    subid : req.params.subid,
                    prosid: req.params.prosid,
                    prodid: req.params.prodid,
                    previd: req.params.id,
                    curr  : req.params.curr,
                    result: result.getlatestvalueResult
                        });
                });             
                db.close();
            });
        });
})};